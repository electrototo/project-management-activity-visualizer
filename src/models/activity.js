import { v4 as uuidv4 } from 'uuid';

export class Activity {
  earlyStart = 0;
  earlyFinish = 0;

  lateStart = 0;
  lateFinish = 0;

  holguraTotal = 0;

  constructor(name="", duration=0, parents=[], children=[], type="normal") {
    this.name = name;
    this.duration = duration;
    this.parents = parents;
    this.children = children;
    this.uuid = uuidv4();
    this.type = type;
  }

  addParent(parent) {
    this.parents.push(parent);
  }

  addChild(child) {
    this.children.push(child);
  }
}
