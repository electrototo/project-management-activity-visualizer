export function calcEarlyStartFinish(graph, node, earlyStart=0) {
  // Depth-first search
  const rootNode = graph[node.uuid];

  // if (rootNode.type === 'finish') {
  //   return;
  // }

  if (earlyStart > rootNode.earlyStart) {
    rootNode.earlyStart = earlyStart;
  }

  rootNode.earlyFinish = rootNode.earlyStart + rootNode.duration;

  for (let i = 0; i < rootNode.children.length; i++) {
    calcEarlyStartFinish(graph, rootNode.children[i], rootNode.earlyFinish);
  }
}
