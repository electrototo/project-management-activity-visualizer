export function calcLateStartFinish(graph, node, lateStart=0, criticalRoute=[]) {
  // Depth-first search
  const rootNode = graph[node.uuid];

  rootNode.lateFinish = rootNode.earlyFinish;
  if (lateStart > rootNode.lateFinish) {
    rootNode.lateFinish = lateStart;
  }

  rootNode.lateStart = rootNode.lateFinish - rootNode.duration;

  if (rootNode.earlyStart === rootNode.lateStart && rootNode.earlyFinish === rootNode.lateFinish) {
    criticalRoute.push(rootNode);
  }

  if (rootNode.type === 'start') {
    return criticalRoute;
  }

  rootNode.holguraTotal = rootNode.lateStart - rootNode.earlyStart;

  for (let i = 0; i < rootNode.parents.length; i++) {
    const currentRoute = calcLateStartFinish(graph, rootNode.parents[i], rootNode.lateStart, criticalRoute);

    if (currentRoute.length > criticalRoute.length) {
      criticalRoute = currentRoute;
    }
  }

  return criticalRoute;
}
