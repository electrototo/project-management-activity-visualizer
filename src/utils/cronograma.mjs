import { calcEarlyStartFinish } from './calc-early-start-finish.mjs';
import { calcLateStartFinish } from './calc-late-start-finish.mjs';
import { Activity } from './activity.mjs';
import { createGraphFromFile } from './create-graph-from-file.mjs';

const activities = createGraphFromFile('./activities.txt');

calcEarlyStartFinish(activities, activities.start);

const criticalRoute = calcLateStartFinish(activities, activities.finish);

// console.log(activities);
console.log(criticalRoute);
