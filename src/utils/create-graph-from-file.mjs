import * as fs from 'fs';
import { Activity } from './activity.mjs';

export function createGraphFromFile(filename) {
  try {
    const data = fs.readFileSync(filename, 'utf8');

    const [definitions, relations] = data.split('===');
    const activities = Object.fromEntries(
      definitions.split('\n').filter(line => line).map((line) => {
        const [name, duration] = line.trim().split(' ');

        return [name, new Activity(name, parseFloat(duration))]
      })
    );

    relations.split('\n').filter(line => line).forEach(line => {
      const [childName, ...parents] = line.split(' ');

      parents.forEach(parentName => {
        activities[childName].addParent(activities[parentName]);
        activities[parentName].addChild(activities[childName]);
      });
    });

    return activities;
  }
  catch (e) {
    return {};
  }
}
