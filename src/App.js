import 'bootstrap/dist/css/bootstrap.min.css';

import React from 'react';
import {
  Switch,
  Redirect,
  Route,
  NavLink
} from 'react-router-dom';
import 'bootstrap';

import { Activity } from './models/activity';
import { calcEarlyStartFinish } from './utils/calc-early-start-finish';
import { calcLateStartFinish } from './utils/calc-late-start-finish';

import Home from './pages/home/Home';
import Activities from './pages/activities/Activities';

class App extends React.Component {
  state = {
    activities: {},
    state: 'clean',
    selectedActivity: null,
    lastInserted: null,
  };

  constructor(props) {
    super(props);

    this.addActivity = this.addActivity.bind(this);
    this.updateActivity = this.updateActivity.bind(this);
    this.createDependency = this.createDependency.bind(this);
    this.removeActivity = this.removeActivity.bind(this);
    this.resetLastInserted = this.resetLastInserted.bind(this);
    this.removeDependencies = this.removeDependencies.bind(this);
  }

  addActivity() {
    const activity = new Activity();

    activity.name = `Activity #${Object.keys(this.state.activities).length + 1}`

    this.setState(state => ({
      activities: {
        [activity.uuid]: activity,
        ...state.activities
      },
      lastInserted: activity.uuid
    }));
  }

  resetLastInserted() {
    this.setState({
      lastInserted: null
    });
  }

  removeActivity(activityId) {
    const activities = Object.assign({}, this.state.activities);

    Object.entries(activities).forEach(([key, activity]) => {
      activity.children = activity.children.filter(child => child.uuid !== activityId);
      activity.parents = activity.parents.filter(parent => parent.uuid !== activityId);
    });

    delete activities[activityId];

    this.setState(state => ({
      activities
    }));
  }

  updateActivity(activityKey, field, value) {
    this.setState(state => ({
      activities: {
        ...state.activities,
        [activityKey]: {
          ...state.activities[activityKey],
          [field]: value
        }
      },
      state: 'dirty'
    }));
  }

  createDependency(childId, parentId) {
    const child = this.state.activities[childId];
    const parent = this.state.activities[parentId];

    this.setState(state => {
      return {
        activities: {
          ...state.activities,
          [childId]: {
            ...child,
            parents: [...child.parents, parent]
          },
          [parentId]: {
            ...parent,
            children: [...parent.children, child]
          }
        },
        state: 'dirty',
      }
    });
  }

  removeDependencies(childId, parents) {
    const activities = Object.assign({}, this.state.activities);

    activities[childId].parents = activities[childId].parents.filter(parent => {
      return !parents.includes(parent.uuid);
    });

    parents.forEach(parentId => {
      activities[parentId].children = activities[parentId].children.filter(child => {
        return child.uuid !== childId;
      })
    });

    this.setState({
      activities
    });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.state.state === 'dirty') {
      const shouldCalcRoute = Object.entries(this.state.activities)
        .filter(([key, activity]) => ['start', 'finish'].includes(activity.type))
        .length === 2;

      if (shouldCalcRoute) {
        const graph = Object.assign({}, this.state.activities);

        const startNode = Object.entries(graph).find(([key, activity]) => activity.type === 'start')[1];
        const finishNode = Object.entries(graph).find(([key, activity]) => activity.type === 'finish')[1];

        calcEarlyStartFinish(graph, startNode);

        const criticalRoute = calcLateStartFinish(graph, finishNode).filter(activity => {
          return !['start', 'finish'].includes(activity.type);
        });

        criticalRoute.forEach(activity => {
          activity.type = 'critical';
        });

        this.setState(state => ({
          state: 'clean',
          activities: graph
        }));
      }
      else {
        this.setState(state => ({ state: 'clean' }));
      }
    }
  }

  render() {
    return (
      <div className="App">
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <div className="container-fluid">
            <NavLink to="/home/" className="navbar-brand">Cristóbal Visualizer</NavLink>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar" aria-controls="navbar">
            </button>
            <div id="navbar" className="collapse navbar-collapse">
              <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <NavLink to="/home/" className="nav-link" activeClassName="active">Home</NavLink>
                <NavLink to="/activities/" className="nav-link" activeClassName="active" onClick={this.resetLastInserted}>Activities</NavLink>
              </ul>
            </div>
          </div>
        </nav>
        <Switch>
          <Route path="/home/">
            <Home
              activities={this.state.activities}
              addActivity={this.addActivity}
              updateActivity={this.updateActivity}
              createDependency={this.createDependency}
              removeActivity={this.removeActivity}
              removeDependencies={this.removeDependencies}
              lastInserted={this.state.lastInserted}
            />
          </Route>
          <Route path="/activities/">
            <Activities activities={this.state.activities} />
          </Route>
          <Route path="/">
            <Redirect to="/home/" />
          </Route>
        </Switch>
      </div>
    );
  }
}

export default App;
