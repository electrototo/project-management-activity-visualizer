import React from 'react';

import ActivityDetail from '../../components/activity-detail/ActivityDetail';


class Activities extends React.Component {
  render() {
    return (
      <div className="container mt-4">
        <div className="row row-cols-3">
          {
            Object.entries(this.props.activities)
              .map(([key, activity]) => {
                return <ActivityDetail key={activity.uuid} activity={activity} />
              })
          }
        </div>
      </div>
    );
  }
}

export default Activities;
