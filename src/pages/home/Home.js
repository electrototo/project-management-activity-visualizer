import React from 'react';

import Sidebar from '../../components/sidebar/Sidebar';
import Visualizer from '../../components/visualizer/Visualizer';


class Home extends React.Component {
  render() {
    return (
      <div className="container mt-4">
        <div className="row">
          <div className="col-3">
            <Sidebar
              activities={this.props.activities}
              addActivity={this.props.addActivity}
              updateActivity={this.props.updateActivity}
              removeActivity={this.props.removeActivity}
              removeDependencies={this.props.removeDependencies}
              createDependency={this.props.createDependency}
              lastInserted={this.props.lastInserted}
            />
          </div>
          <div className="col-9" style={{height: "90vh"}}>
            <Visualizer
              activities={this.props.activities}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
