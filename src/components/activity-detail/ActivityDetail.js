import React from 'react';


class ActivityDetail extends React.Component {
  render() {
    const { activity } = this.props;

    return (
      <div className="col mb-4">
        <div className="card">
          <div className="card-header">
            {this.props.activity.name}
          </div>
          <div className="card-body">
            <table className="table">
              <tbody>
                <tr>
                  <th scope="row">Duration</th>
                  <td>{activity.duration}</td>
                </tr>
                <tr>
                  <th scope="row">Early start</th>
                  <td>{activity.earlyStart}</td>
                </tr>
                <tr>
                  <th scope="row">Late start</th>
                  <td>{activity.lateStart}</td>
                </tr>
                <tr>
                  <th scope="row">Early finish</th>
                  <td>{activity.earlyFinish}</td>
                </tr>
                <tr>
                  <th scope="row">Late finish</th>
                  <td>{activity.lateFinish}</td>
                </tr>
                <tr>
                  <th scope="row">Total float</th>
                  <td>{activity.holguraTotal}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default ActivityDetail;
