import React from 'react';

import ActivityForm from '../activity-form/ActivityForm';

class Sidebar extends React.Component {
  render() {
    return (
      <div>
        <div className="d-grip gap-2">
          <button className="btn btn-primary col-12 mb-4"
            type="button" onClick={this.props.addActivity}>
            + New activity
          </button>
        </div>
        <div className="accordion overflow-auto" id="form-accordion">
          {
            Object.keys(this.props.activities).length !== 0 ? 
              Object.entries(this.props.activities).map(([key, value], index) => {
                return (
                  <ActivityForm
                    key={key}
                    activity={value}
                    activities={this.props.activities}
                    updateActivity={this.props.updateActivity}
                    removeActivity={this.props.removeActivity}
                    removeDependencies={this.props.removeDependencies}
                    createDependency={this.props.createDependency}
                    collapse={this.props.lastInserted !== key}
                  />
                )
              }) :
              <div className="card">
                <div className="card-body">
                  <p>No activities found. <button onClick={this.props.addActivity} style={{
                    "backgroundColor": "transparent",
                    color: "blue",
                    border: "none",
                    cursor: "pointer",
                    "textDecoration": "underline",
                    display: "inline",
                    margin: 0,
                    padding: 0
                  }}>Create a new one</button>.</p>
                </div>
              </div>
          }
        </div>
      </div>
    );
  }
}

export default Sidebar;
