import React from 'react';
import Select from 'react-select';

class ActivityForm extends React.Component {
  constructor(props) {
    super(props);

    this.accId = "accordion-" + props.activity.uuid;

    this.handleOnChange = this.handleOnChange.bind(this);
    this.handleDependencies = this.handleDependencies.bind(this);
  }

  handleOnChange(event) {
    const name = event.target.name;
    let value = event.target.value;

    if (name === 'duration') {
      value = parseFloat(value);
    }

    this.props.updateActivity(
      this.props.activity.uuid,
      name,
      value
    );
  }

  handleDependencies(values, { action, option }) {
    console.log(action, values, option);
    if (action === 'select-option') {
      this.props.createDependency(this.props.activity.uuid, option.value);
    }
    else if (['remove-value', 'pop-value', 'clear'].includes(action)) {
      const valuesIds = values.map(data => data.value);
      const activitiesToRemove = this.props.activity.parents.filter(parent => {
        return !valuesIds.includes(parent.uuid);
      }).map(activity => activity.uuid);

      this.props.removeDependencies(this.props.activity.uuid, activitiesToRemove);
    }
  }

  render() {
    return (
      <div className="accordion-item">
        <h2 className="accordion-header">
          <button
            className={"accordion-button" + (this.props.collapse ? " collapsed" : "")} type="button" data-bs-toggle="collapse"
            data-bs-target={"#" + this.accId} aria-expanded="true" aria-controls="">
            { this.props.activity.name }
          </button>
        </h2>
        <div id={this.accId} className={"accordion-collapse collapse" + (!this.props.collapse ? " show" : "")} data-bs-parent="#form-accordion">
          <div className="accordion-body">
            <form>
              <div className="mb-2">
                <label htmlFor="">Name</label>
                <input className="form-control" type="text" name="name"
                       onChange={this.handleOnChange} value={this.props.activity.name} />
              </div>
              <div className="mb-2">
                <label htmlFor="">Duration</label>
                <input className="form-control" type="number" name="duration"
                       onChange={this.handleOnChange} value={this.props.activity.duration} />
              </div>
              <div className="mb-2">
                <label htmlFor="">Dependencies</label>
                  <Select
                    options={
                      Object.entries(this.props.activities).filter(([key, activity]) => {
                        return key !== this.props.activity.uuid;
                      }).map(([key, value]) => {
                        return {
                          value: key,
                          label: value.name,
                        };
                      })
                    }
                    value={
                      this.props.activity.parents.map(parent => ({
                        value: parent.uuid,
                        label: parent.name,
                      }))
                    }
                    isMulti={true}
                    onChange={this.handleDependencies}
                  />
              </div>
              <div className="mb-2">
                <label htmlFor="" className="mb-2">Type of activity</label>
                <div className="form-check">
                  <input type="radio" className="form-check-input" name="type"
                    value="normal" onChange={this.handleOnChange}
                    checked={['normal', 'critical'].includes(this.props.activity.type) ? 'checked' : ''} />

                  <label htmlFor="" className="form-check-label">Normal</label>
                </div>
                <div className="form-check">
                  <input type="radio" className="form-check-input" name="type"
                    value="start" onChange={this.handleOnChange}
                    checked={this.props.activity.type === 'start' ? 'checked' : ''} />

                  <label htmlFor="" className="form-check-label">Start</label>
                </div>
                <div className="form-check">
                  <input type="radio" className="form-check-input" name="type"
                    value="finish" onChange={this.handleOnChange}
                    checked={this.props.activity.type === 'finish' ? 'checked' : ''} />

                  <label htmlFor="" className="form-check-label">Finish</label>
                </div>
              </div>
              <div className="row">
                <button className="btn btn-danger" type="button" onClick={() => this.props.removeActivity(this.props.activity.uuid)}>Delete</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ActivityForm;
