import React from 'react';

import { Network } from 'vis-network/peer';
import 'vis-network/styles/vis-network.css';

class Visualizer extends React.Component {
  constructor(props) {
    super(props);

    this.ref = React.createRef();
    this.handleNodeClick = this.handleNodeClick.bind(this);
    this.calcNodesEdges = this.calcNodesEdges.bind(this);
  }

  render() {
    return (
      <div ref={this.ref} style={{height: "100%", border: "1px dotted"}}></div>
    );
  }

  componentDidMount() {
    const options = {
      nodes: {
        shape: "dot"
      },
      edges: {
        width: 2,
        arrows: "to",
      },
      groups: {
        critical: {
          color: 'rgba(255, 0, 0, 0.5)'
        },
        start: {
          color: 'rgba(0, 0, 255, 0.5)'
        },
        finish: {
          color: 'rgba(0, 0, 255, 0.5)'
        },
        normal: {
          color: 'rgba(0, 0, 255, 0.5)'
        }
      },
    };

    const [nodes, edges] = this.calcNodesEdges();

    this.network = new Network(this.ref.current, {nodes, edges}, options);
    this.network.on('click', this.handleNodeClick);
  }

  handleNodeClick(params) {
    if (params.nodes.length === 1) {
    }
  }

  createLabel(activity) {
    let label = `${activity.name}`;

    return label;
  }

  calcNodesEdges() {
    const nodes = [];
    const edges = [];

    Object.entries(this.props.activities).forEach(([key, activity]) => {
      nodes.push({
        id: key,
        label: this.createLabel(activity),
        title: activity.name,
        group: activity.type,
      });

      activity.children.forEach(child => {
        edges.push({
          from: key,
          to: child.uuid,
        });
      });
    });

    return [nodes, edges];
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const [nodes, edges] = this.calcNodesEdges();

    this.network.setData({
      nodes, edges
    });
  }
}

export default Visualizer;
